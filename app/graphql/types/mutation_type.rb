Types::MutationType = GraphQL::ObjectType.define do
  name "Mutation"

  field :createUser, function: Mutations::Users::CreateUser.new
  field :updateUser, function: Mutations::Users::UpdateUser.new
  field :destroyUser, function: Mutations::Users::DestroyUser.new
end