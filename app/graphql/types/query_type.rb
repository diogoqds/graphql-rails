Types::QueryType = GraphQL::ObjectType.define do
  name "Query"
  # Add root-level fields here.
  # They will be entry points for queries on your schema.
  field :user, Types::UserType do
    argument :id, types.ID

    resolve -> (obj, args, context) {
      User.where(id: args[:id]).first
    }
  end

  field :allUsers, types[Types::UserType] do
    resolve -> (obj, args, context) {
      User.all.order(id: :asc)
    }
  end

  field :country, Types::CountryType do
    argument :id, types.ID

    resolve -> (obj, args, context) {
      Country.find(args[:id])
    }
  end

  field :allCountries, types[Types::CountryType] do
    resolve -> (obj, args, context) {
      Country.all
    }
  end
end
