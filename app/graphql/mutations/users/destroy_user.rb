class Mutations::Users::DestroyUser < GraphQL::Function
  argument :id,             !types.ID

  type types.Boolean

  def call(obj, args, context)
    User.destroy(args[:id])
    true
  end
end